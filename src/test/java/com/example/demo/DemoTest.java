package com.example.demo;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.matchers.Times.exactly;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.client.methods.HttpGet;
import java.io.ByteArrayOutputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;
import javax.net.ssl.SSLContext;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TestInstance(Lifecycle.PER_CLASS)
class DemoTest {

	private ClientAndServer mockServer;
	
	private String mockServerHost = "localhost";
	private int mockServerPort = 1080;
	private String mockServerUrl = "http://" + mockServerHost + ":" + mockServerPort;
	
	private MockServerClient client = new MockServerClient(mockServerHost,mockServerPort);


	@BeforeAll
    public void startServer() {
        mockServer = ClientAndServer.startClientAndServer(1080);
    }
	
    @AfterAll 
    public void stopServer() { 
        mockServer.stop();
    }

	@Test
	void contextLoads() {
    //Configuring mock
		client
        .when(
          request()
            .withMethod("GET")
            .withPath("/hello"),
            exactly(1))
              .respond(
                response()
                  .withStatusCode(200)
                  .withHeaders(
                    new Header("Content-Type", "text/xml;charset=UTF-8"))
                  .withBody("Hello World!")
                  .withDelay(TimeUnit.SECONDS,1)
              );
		System.out.println("IT WORKED!");

    //Checking if request works!
	try{
		CloseableHttpClient client = configHTTPClient();
		HttpGet httpGet = new HttpGet("http://localhost:1080/hello");
		CloseableHttpResponse response = client.execute(httpGet);

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		response.getEntity().writeTo(buffer);
		String payload = buffer.toString(java.nio.charset.StandardCharsets.UTF_8.name());
		System.out.println("PAYLOAD: " + payload);
	} catch(Exception e){
		System.out.println("EXCEPTION OCCURED");
	}
    
    Assertions.assertTrue(42 == 42);
	}

  private CloseableHttpClient configHTTPClient() {

		try {
			TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
			SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,
					NoopHostnameVerifier.INSTANCE);

			Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
					.register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					socketFactoryRegistry);
			return HttpClients.custom().setSSLSocketFactory(sslsf).setConnectionManager(connectionManager).build();
		} catch (KeyManagementException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}

